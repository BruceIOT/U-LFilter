#include <iostream>  
#include <string>  
#include <vector>
#include <fstream>  
#include <sstream>
#include <algorithm>
#include <cctype>
#include <Windows.h>
#include "FilterDlg.h"
#include <stdio.h> 
#include <io.h>
#include "SaveToXsl.h"
using namespace std;

////////声明字符串全局性常量
string csvFilename = "";//默认csv文件名为空
int selectCurve =0;//设置默认滤波曲线为压边力,全局变量
short int enlargeYaxis =0;//设置默认缩小Y轴的数值显示范围,0代表缩小，1代表放大数值显示范围
//检测是否有csv文件函数，返回检测到的csv文件名
const string CFilterDlg::*getDAQname()
{
	int i;
	int j = 0;
	CFileFind finder;
	vector<CString>allfile;//创建字符串容器
	vector<CString>getfile;
	//不断反复地读取当前目录，读到csv后缀文件时进行下一步
	while (true)
	{
		bool bworking = finder.FindFile(_T("./*.csv"));//查询该文件夹下的所有文件
		if (bworking)
		{
			bworking = finder.FindNextFile();
			allfile.push_back(finder.GetFileName());   //得到所有文件，文件夹的名称，存在allfile字符串容器里面
			break;
		}
	}
	for (i = 0; i < allfile.size(); i++)          //从allfile字符串容器中过滤出".bmp"后缀的文件
	{
		int flag = allfile[i].Find(_T(".csv"));//有无csv文件标志，如果大于0则出现csv文件
		if (flag >= 0)
		{
			//printf("%s\n", allfile[i]);
			//getfile.push_back(allfile[i]);         //将后缀符合要求的文件写到getfile字符串容器里面
			csvFilename = allfile[i].GetBuffer(0);
			//申请控制台调试
			AllocConsole();// 打开控制台资源
			freopen("CONOUT$", "w+t", stdout);// 申请写
			freopen("CONIN$", "r+t", stdin);// 申请读
			cout << "软件此时打开的CSV文件名称为：" << csvFilename << endl;//用cmd测试显示用
			FreeConsole();// 释放控制台资源
			break;
		}
	}
	finder.Close();
	return NULL;
}

//循环队列只能用（circleQueueSize-1）个数据
const int circleQueueSize = 12;
typedef struct SqQueue
{
	float queueData[circleQueueSize];
	int head;/////头指针
	int tail; ////尾指针
}SqQueue;

/////链表结构体
typedef struct pointData
{
	int pointOrder;//节点序号，数值从0开始
	int pointNumber;//自带节点ID，数值不从0开始
	string pointTime;//节点时间
	float sensorData0;//压边力数据
	float sensorData1;//弯曲力数据
	float sensorData2;//冗余数据0
	float sensorData3;//冗余数据1
}pointData;

typedef struct listpoint
{
	pointData *information;
	listpoint *next;
	listpoint *last;
}listpoint;

///////链表数据结构相关函数
listpoint *push_tail(listpoint *tail, int pointOrder, int pointNumber, const string &pointTime, float sensorData0, float sensorData1, float sensorData2, float sensorData3)
{
	/////头结点无数据信息
	listpoint *tail1;
	tail1 = tail;
	listpoint *insertpoint;
	pointData *dataPointer;
	insertpoint = new listpoint;
	dataPointer = new pointData;
	/////加入节点数据信息
	dataPointer->pointOrder = pointOrder;
	dataPointer->pointNumber = pointNumber;
	dataPointer->pointTime = pointTime;
	dataPointer->sensorData0 = sensorData0;
	dataPointer->sensorData1 = sensorData1;
	dataPointer->sensorData2 = sensorData2;
	dataPointer->sensorData3 = sensorData3;
	insertpoint->information = dataPointer;
	///更新节点地址
	tail1->next = insertpoint;
	insertpoint->last = tail1;
	insertpoint->next = NULL;
	return insertpoint;
}

void output_point(listpoint *point)
{
	while ((point->next) != NULL)
	{
		cout << "pointNumber :" << point->information->pointNumber;
		cout << "\tpointTime:" << point->information->pointTime;
		cout << "\tsensorData0 :" << point->information->sensorData0;
		cout << "\tsensorData1 :" << point->information->sensorData1 << endl << endl;
		point = point->next;
	}
}

////////////c++字符串分割函数，将字符串S用const string& c符号进行分割。////////
void SplitString(const string& s, vector<string>& v, const string& c)
{
	string::size_type pos1, pos2;
	pos2 = s.find(c);
	pos1 = 0;
	while (string::npos != pos2)
	{
		v.push_back(s.substr(pos1, pos2 - pos1));
		pos1 = pos2 + c.size();
		pos2 = s.find(c, pos1);
	}
	if (pos1 != s.length())
	{
		v.push_back(s.substr(pos1));
	}
}

//////计算CSV文件总行数，总列数等相关信息,返回int型（行列数）数组
int * getLineAmount(string fileName) {
	//////申明行、列数变量
	int rowAmount = 0;
	int columnAmount = 0;
	/////csv文件的行列数，行数为lineAmount[0],列数lineAmount[1]。
	static int lineAmount[5];
	/////////////////////读取csv数据文件////////////////////////
	ifstream inFile(fileName.c_str(), ios::in);
	string lineStr;
	vector<string>;
	int j = 0;
	char* end;
	if (inFile.fail())
	{
		cout << "读取文件失败" << endl;
	}
	/////设置存储csv文件中字符串的数组变量
	while (getline(inFile, lineStr))
	{
		if (rowAmount == 15)///以15行数据的列数作为整个项目的列数。
		{
			// ///////////////////对字符串过滤引号////////////////////
			std::string filterString = lineStr;
			filterString.erase(std::remove(filterString.begin(), filterString.end(), '"'), filterString.end());
			//////////////////////通过逗号对字符串剪切。////////////////////////////
			vector<string> stringVector;
			SplitString(filterString, stringVector, ","); //可按多个字符来分隔
			for (vector<string>::size_type j = 0; j != stringVector.size(); ++j)
			{
				columnAmount++;
			}
		}
		rowAmount++;
	}
	lineAmount[0] = rowAmount;
	lineAmount[1] = columnAmount;
	return lineAmount;
}

////////将csv文件数据制作成链表数据结构（原始CSV文件数据结构）
listpoint *listSensorData(int lineNodeAmount) {
	int rowAmount = lineNodeAmount;//////DAQcsv文件总行数 
	/////////////////////读取csv数据文件////////////////////////
	ifstream inFile(csvFilename.c_str(), ios::in);
	string lineStr;
	vector<string>;
	char* end;
	if (inFile.fail())
	{
		cout << "读取文件失败" << endl;
	}
	/////设置存储csv文件中字符串的数组变量
	const int nodeAmount = rowAmount - 20;//去掉csv文件开头和末尾十行，所以链表数据节点数为rowAmount-20
	int i, j;
	i = 0, j = 0;
	///初始化链表相关变量
	int pointNumber = 0;
	string datatime;
	float sensordata0 = 0;
	float sensordata1 = 0;
	float sensordata2 = 0;
	float sensordata3 = 0;
	int pointOrder = 0;
	listpoint *dataHead, *pushTail;
	dataHead = (listpoint*)malloc(sizeof(listpoint));
	pointData *headinfo;
	headinfo = new pointData;//DAQ的csv链表的头结点无节点数据
	headinfo->pointNumber = nodeAmount;////将链表节点数存入链表头的pointNumber中
	dataHead->information = headinfo;
	pushTail = dataHead;
	while (getline(inFile, lineStr))
	{
		if (i > 9)/////第一个满足条件的i为10
		{
			////////////////////过滤引号字符串////////////////
			std::string filterString = lineStr;
			filterString.erase(std::remove(filterString.begin(), filterString.end(), '"'), filterString.end());
			//////////////////////剪切字符串中逗号。////////////////////////////
			vector<string> stringVector;
			SplitString(filterString, stringVector, ","); //可按多个字符来分隔;
			for (vector<string>::size_type j = 0; j != stringVector.size(); ++j)//将每一行的各种数据放入节点中转变量中
			{
				switch (j)
				{
				case 0:
					pointNumber = atoi(stringVector[j].c_str());//节点ID值，数值不从0开始
					break;
				case 1:
					datatime = stringVector[j].c_str();//节点时间
					break;
				case 2:
					sensordata0 = atof(stringVector[j].c_str());//压边力数据
					break;
				case 3:
					sensordata1 = atof(stringVector[j].c_str());//弯曲力数据
					break;
				case 4:
					sensordata2 = atof(stringVector[j].c_str());//冗余数据0
					break;
				case 5:
					sensordata3 = atof(stringVector[j].c_str());//冗余数据1
					break;
				}
				pointOrder = i - 10;//序号从0开始
			}
	pushTail = push_tail(pushTail, pointOrder, pointNumber, datatime, sensordata0, sensordata1, sensordata2, sensordata3);
		}
		/////设置断点，去掉csv文件最后十行
		if (i >= (rowAmount - 10))
		{
			break;
		}
		i++;
	}
	////返回链表头节点
	return dataHead;
}

//从链表头到尾，求原始数据链表的第一个最大值
listpoint* getCSVMax(listpoint *filterListHead)
{
	//如果链表为NULL 则返回 NULL
	if (filterListHead == NULL) {
		return NULL;
	}
	//如果链表只有头结点，则也返回NULL
	else if (filterListHead->information->sensorData0 == NULL) {
		return NULL;
	}
	else
	{
		listpoint *p, *maxNode;
		p = filterListHead->next;
		maxNode = p;//maxp指向第一个节点
		while (p != NULL)
		{
			if ((p->information->sensorData0) > (maxNode->information->sensorData0))
			{
				maxNode = p;
			}
			p = p->next;
		}
		return maxNode;//返回最大值链表节点
	}
}

//求原始链表的最小值,且有多个相同的min时，用search的第一个值作为最小值
listpoint* getCSVMin(listpoint *filterListHead)
{
	//如果链表为NULL 则返回 NULL
	if (filterListHead == NULL) {
		return NULL;
	}
	//如果链表只有头结点，则也返回NULL
	else if (filterListHead->next == NULL) {
		return NULL;
	}
	else
	{
		listpoint *p, *minNode;
		p = filterListHead->next;
		minNode = p;//max指向第一个节点
		while (p != NULL)
		{
			if ((p->information->sensorData0)<(minNode->information->sensorData0))
			{
				minNode = p;
			}
			p = p->next;
		}
		return minNode;//返回最小值节点
	}
}

//相邻平均相关滤波算法。
//求队列长度
int QueueLength(SqQueue *s)
{
	return (s->head-s->tail + circleQueueSize) % circleQueueSize;
}

//元素入队
bool pushQueue(SqQueue *s, float e)
{
	//如果队列满
	if ((s->head + 1) % circleQueueSize == s->tail)
	{
		return false;
	}
	else
	{
		s->queueData[s->tail] = e;	//将元素赋值给队尾
		s->tail = (s->tail + 1) % circleQueueSize;	//tail指针向后移一位，若到最后则转到数组头部
		return true;
	}
}

//元素出队
float outQueue(SqQueue *s) {
	float outData;
	if (s->head == s->tail)
	{
		return false;
	}
	else
	{
		outData = s->queueData[s->head];	//将队头元素赋值给outData
		s->head = (s->head + 1) % circleQueueSize;//head指针向后移一位，若到最后则转到数组头部
		return outData;
	}
}

//滤波后数据用的链表数据结构，相邻平均法相关数据结构和函数
typedef struct dataList
{
	int pointOrder;//节点序号，从0开始
	int pointNumber;//节点ID值，不从0开始
	string pointtime;//滤波时间点值
	float bhfOldData;//压边力原始值
	float bhfFilterData;//压边力滤波值
	float oldNodeData;//弯曲力原始值
	float filterNodeData;//弯曲力的滤波值
	dataList *nextNode;//下一个相邻平局滤波链表的地址指针
}dataList;

////压入过滤后数据
dataList *PushFilterData(listpoint *listTail,dataList *filterTail,int pointOrder,float bhfFilterData ,float bendNodeValue)
{
	dataList *newNode = new dataList;
	newNode->pointOrder = pointOrder;
	newNode->pointNumber = listTail->information->pointNumber;
	newNode->pointtime = listTail->information->pointTime;
	//传入压边力、弯曲力的数据
	newNode->bhfOldData = listTail->information->sensorData0;//压边力数据
	newNode->bhfFilterData = bhfFilterData;
	newNode->oldNodeData = listTail->information->sensorData1;//弯曲力数据
	newNode->filterNodeData = bendNodeValue;
	filterTail->nextNode = newNode;///将当前的尾节点和new的表节点连起来
	newNode->nextNode = NULL;//最后一个节点的next指针为Null，方便使用。
	return newNode;//返回新建的节点地址值
}

///计算滤波链表
dataList *calDataList(listpoint *calListHead) {
	listpoint *oldList = new listpoint;
	//运用相邻平均法时，本项目的CSV数据链表头结点没有数据，需要listHead->next
	oldList = calListHead->next;
	SqQueue *bhfCircleQ = new SqQueue;//创建压边力过滤循环队列，并设置头尾指针
	bhfCircleQ->head = 0;
	bhfCircleQ->tail = 11;
	SqQueue *circleQ = new SqQueue;//创建弯曲力过滤循环队列，并设置头尾指针
	circleQ->head = 0;
	circleQ->tail = 11;
	int i = 0;
	//对弯曲力进行过滤
	//用链表首先初始化队列十一个数据,且for最后oldList指向第十二数据，即后面可以直接用oldList数据地址值
	for (i = 0; i <= (circleQueueSize - 2); i++)
	{
		bhfCircleQ->queueData[i]= oldList->information->sensorData0;//将原始压边力数据传入压边力滤波循环队列
		circleQ->queueData[i] = oldList->information->sensorData1;//将原始弯曲力数据传入循环队列
		oldList = oldList->next;
	}
	//下面开始滤波
	dataList *smoothListHead = new dataList;
	//初始化压边力、弯曲力数据
	smoothListHead->bhfOldData = 0.000;
	smoothListHead->bhfFilterData = 0.000;
	smoothListHead->oldNodeData = 0.000;
	smoothListHead->filterNodeData = 0.000;
	dataList *smoothListTail;
	smoothListTail = smoothListHead;
	float bhfFilterData = 0;//初始化压边力滤波数据
	float bendFilterData = 0;//初始化弯曲力滤波数据
	//窗口值为11，所以将滤波后的链表前五个值初始化为0.
	for (i = 0; i <=4;i++)
	{
		smoothListTail = PushFilterData(oldList, smoothListTail,i,0,0);
	}
	//计算队列中数据的平均数，将平均数压入filter链表
	while ((oldList->next) != NULL)
	{
		float bhfQueueSum = 0;
		float bendQueueSum = 0;
		//计算循环队列中各项数据的平均值
		for (int j = 0; j < circleQueueSize -1; j++)
		{
			bhfQueueSum = bhfQueueSum + bhfCircleQ->queueData[j];//计算压边力循环队列中的和
			bendQueueSum = bendQueueSum + circleQ->queueData[j];//计算弯曲力循环队列中的和
		}
		bhfFilterData= bhfQueueSum/ (circleQueueSize - 1);//压边力滤波值
		bendFilterData = bendQueueSum / (circleQueueSize - 1);//弯曲力滤波值
		//将计算得到平均值插入到滤波链表
		smoothListTail = PushFilterData(oldList, smoothListTail, i,bhfFilterData, bendFilterData);
		//从第12个数据节点开始，将需要滤波的数据不断传入队列，再计算平均值
		//压边力循环队列出队、入队
		outQueue(bhfCircleQ);
		pushQueue(bhfCircleQ, oldList->information->sensorData0);
		//弯曲力循环队列出队、入队
		outQueue(circleQ);//将弯曲力队列头元素出队
		pushQueue(circleQ, oldList->information->sensorData1);//在队尾加入弯曲力原始数据
		oldList = oldList->next;
		i++;
	}
	return smoothListHead;////返回滤波后链表的表头节点。
}

//////链表顺序查找法
listpoint *serchCsvList(listpoint *listHead, const string &serchValue) {
	listpoint *serchNode = new listpoint;
	serchNode = listHead->next;
	while (serchNode->next != NULL)///此时不能查找最后一个节点值
	{
		if (serchNode->information->pointTime == serchValue)
		{
			return serchNode;///返回查找到的节点值
		}
		else
		{
			serchNode = serchNode->next;
		}
	}
	return NULL;
}

/////求进行相邻平均滤波后链表的最大值
dataList* getFilterMax(dataList *filterListHead)
{
	//如果链表为NULL 则返回 NULL
	if (filterListHead == NULL) {
		return NULL;
	}
	//如果链表只有头结点，则也返回NULL
	else if (filterListHead->nextNode == NULL) {
		return NULL;
	}
	else
	{
		dataList *p, *maxNode;
		p = filterListHead->nextNode;
		maxNode = p;//maxp指向第一个节点
		while (p != NULL)
		{
			if ((p->filterNodeData) > (maxNode->filterNodeData)) {
				maxNode = p;
			}
			p = p->nextNode;
		}
		return maxNode;//返回最大值链表节点
	}
}

/////求相邻平均滤波后链表的最小值
dataList* getFilterMin(dataList *filterListHead)
{
	//如果链表为NULL 则返回 NULL
	if (filterListHead == NULL) {
		return NULL;
	}
	//如果链表只有头结点，则也返回NULL
	else if (filterListHead->nextNode == NULL) {
		return NULL;
	}
	else
	{
		dataList *p, *minNode;
		p = filterListHead->nextNode;
		minNode = p;//max指向第一个节点
		while (p != NULL)
		{
			if (p->filterNodeData < minNode->filterNodeData)
			{
				minNode = p;
			}
			p = p->nextNode;
		}
		return minNode;//返回最小值节点
	}
}

////////////调用Python脚本，将原本的CSV数据通过S-G滤波算法进行滤波
void CFilterDlg::calCSVS_G() {
	//首先查看是否存在useData文件夹
	char* filePath = "useData\\";
	if (_access(filePath,0)==-1)///如果没有useData文件夹
	{
		CString strFileName = "useData\\";//创建useData文件夹
		cout << "正在计算S_G滤波算法所需文件\n正在计算S_G滤波算法所需文件" << endl;
		CreateDirectory((LPCTSTR)strFileName, NULL);
		///将csv文件名当作字符串传入python脚本
		string command_rd = "python S-GFilter.py \"" +csvFilename+"\" \\";
		system(command_rd.c_str());
		return;
	}
	//如果useData文件夹存在，再检查是否存在afterFilter.csv文件
	fstream isExistFile;
	isExistFile.open(".\\useData\\afterFilter.csv", ios::in);
	if (!isExistFile)//如果不存在
	{
		cout << "正在计算S_G滤波算法所需文件\n正在计算S_G滤波算法所需文件" << endl;
		//将csv文件名当作字符串传入python脚本,计算出s-g滤波csv文件
		string command_rd = "python S-GFilter.py \"" + csvFilename +"\" \\";
		system(command_rd.c_str());
	}
	return;
}

////////////////////////读取用Python进行S-G滤波后的CSV文件///////////////////////
////S-G滤波所用数据结构
typedef struct s_gList {
	int pointOrder;
	int pointNumber;
	string pointTime;
	float bhfOriData;//压边力原数据
	float bhfFilterData;//压边力滤波数据
	float oringinData;//弯曲力元数据
	float filterData;//弯曲力滤波数据
	s_gList *lastNode;
	s_gList *nextNode;
}s_gList;

const string s_gCsvFilename = "./useData/afterFilter.csv";
s_gList *getS_GListCSV() {

	ifstream inFile(s_gCsvFilename.c_str(), ios::in);
	string lineStr;
	vector<string>;
	char* end;
	if (inFile.fail())
	{
		cout << "读取文件失败" << endl;
	}
	int i, j;
	i = 0, j = 0;
	///初始化链表相关变量
	int pointOrder = 0;
	int pointNumber = 0;
	string datatime;
	float bhfOriData = 0;//压边力
	float bhfFilterData = 0;
	float oringinData = 0;//弯曲力
	float filterData = 0;
	s_gList *listHead, *listTail;
	listHead = new s_gList;
	listHead->pointOrder;
	listTail = listHead;//s-g滤波链表头结点也没有数据
	while (getline(inFile, lineStr))
	{
		if (i > 0)/////去除s_g滤波文件的第一行
		{
			////////////////////过滤引号字符串////////////////
			std::string filterString = lineStr;
			filterString.erase(std::remove(filterString.begin(), filterString.end(), '"'), filterString.end());
			//////////////////////剪切字符串中逗号。////////////////////////////
			vector<string> stringVector;
			SplitString(filterString, stringVector, ","); //可按多个字符来分隔;
			for (vector<string>::size_type j = 0; j != stringVector.size(); ++j)
			{
				switch (j)
				{
				case 0:
					pointOrder = atoi(stringVector[j].c_str());//滤波序号，从0开始自增
					break;
				case 1:
					pointNumber = atoi(stringVector[j].c_str());//原始DAQcsv序号，数值不从0开始自增1
					break;
				case 2:
					datatime = stringVector[j].c_str();//节点时间
					break;
				case 3://压边力原数据
					bhfOriData = atof(stringVector[j].c_str());
					break;
				case 4://压边力滤波数据
					bhfFilterData = atof(stringVector[j].c_str());
					break;
				case 5://弯曲力原数据
					oringinData = atof(stringVector[j].c_str());
					break;
				case 6://弯曲力滤波数据
					filterData = atof(stringVector[j].c_str());
					break;
				}
			}
			//////将得到的数据放入链表节点
			s_gList *newDataNode = new s_gList;////申请滤波链表节点内存空间
			newDataNode->pointOrder = pointOrder;
			newDataNode->pointNumber = pointNumber;
			newDataNode->pointTime = datatime;
			newDataNode->bhfOriData = bhfOriData;//压边力元数据
			newDataNode->bhfFilterData = bhfFilterData;
			newDataNode->oringinData = oringinData;//弯曲力元数据
			newDataNode->filterData = filterData;
			listTail->nextNode = newDataNode;
			newDataNode->lastNode = listTail;
			newDataNode->nextNode = NULL;
			listTail = newDataNode;
		}
		i++;
	}
	////返回链表头节点，但头结点无数据
	return listHead;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CSaveToXsl::CSaveToXsl()
{
	m_strH = "";
	m_strFileName = "";
}

CSaveToXsl::~CSaveToXsl()
{
	if (m_database.IsOpen())
		m_database.Close();
}

void CSaveToXsl::GetFileName(CString strFileName)
{
	///默认文件名
	if (strFileName == "")
		GetDefaultXlsFileName(m_strFileName);
	else
		m_strFileName = strFileName;

}

void CSaveToXsl::Initialize(CString strTitle/*表名*/, CString strCreateName/*字段名*/, CString strName)
{	//strNames = "索引 TEXT,方向 TEXT,时间 TEXT" ；
	CString warningStr;
	CString sDriver;
	CString sSql = ""; //SQL 语句
	m_tableName = strTitle;
	m_strH = strName;  //保存字段名	

	// 检索是否安装有Excel驱动 "Microsoft Excel Driver (*.xls)" 
	sDriver = GetExcelDriver();
	if (sDriver.IsEmpty())
	{
		// 没有发现Excel驱动
		AfxMessageBox("没有安装Excel!\n请先安装Excel软件才能使用导出功能!");
		return;
	}

	if (m_strFileName == "")
	{
		GetFileName();
	}
	// 创建进行存取的字符串
	sSql.Format("DRIVER={%s};DSN='';FIRSTROWHASNAMES=1;READONLY=FALSE;CREATE_DB=\"%s\";DBQ=%s", sDriver, m_strFileName, m_strFileName);

	// 创建数据库 (既Excel表格文件)
	if (m_database.OpenEx(sSql, CDatabase::noOdbcDialog))
	{
		// 创建表结构
		sSql = "CREATE TABLE " + m_tableName + " ( " + strCreateName + " ) ";
		m_database.ExecuteSQL(sSql);
	}
}

BOOL CSaveToXsl::InsertToTable(int pointNumber, CString pointTime, float DAQdata)
{
	// strV = " '10' ,'0', '2006:10:15' " ;
	if (!m_database.IsOpen())
	{
		AfxMessageBox("数据库未打开");
		return FALSE;
	}
	CString sSql = "";
	sSql.Format("insert into " + m_tableName + "(" + m_strH + ") values ('%d','%s','%f')", pointNumber, pointTime, DAQdata);
	m_database.ExecuteSQL(sSql);
	return TRUE;
}

CString CSaveToXsl::GetExcelDriver()
{
	char szBuf[2001];
	WORD cbBufMax = 2000;
	WORD cbBufOut;
	char *pszBuf = szBuf;
	CString sDriver;

	// 获取已安装驱动的名称(函数在odbcinst.h里)
	if (!SQLGetInstalledDrivers(szBuf, cbBufMax, &cbBufOut))
		return "";

	// 检索已安装的驱动是否有Excel...
	do
	{
		if (strstr(pszBuf, "Excel") != 0)
		{
			//发现 !
			sDriver = CString(pszBuf);
			break;
		}
		pszBuf = strchr(pszBuf, '\0') + 1;
	} while (pszBuf[1] != '\0');
	return sDriver;
}

BOOL CSaveToXsl::MakeSurePathExists(CString &Path, bool FilenameIncluded)
{
	int Pos = 0;
	while ((Pos = Path.Find('\\', Pos + 1)) != -1)
		CreateDirectory(Path.Left(Pos), NULL);
	if (!FilenameIncluded)
		CreateDirectory(Path, NULL);
	//	return ((!FilenameIncluded)?!_access(Path,0):
	//	!_access(Path.Left(Path.ReverseFind('\\')),0));
	return !_access(Path, 0);
}

//获得默认的文件名
BOOL CSaveToXsl::GetDefaultXlsFileName(CString& sExcelFile)
{
	///默认文件名：yyyymmddhhmmss.xls
	CString timeStr;
	CTime day;
	day = CTime::GetCurrentTime();
	int filenameday, filenamemonth, filenameyear, filehour, filemin, filesec;
	filenameday = day.GetDay();//dd
	filenamemonth = day.GetMonth();//mm月份
	filenameyear = day.GetYear();//yyyy
	filehour = day.GetHour();//hh
	filemin = day.GetMinute();//mm分钟
	filesec = day.GetSecond();//ss
	timeStr.Format("%04d-%02d-%02d %02d.%02d.%02d", filenameyear, filenamemonth, filenameday, filehour, filemin, filesec);
	sExcelFile = timeStr + ".xls";
	// prompt the user (with all document templates)
	CFileDialog dlgFile(FALSE, ".xls", sExcelFile);
	CString title;
	CString strFilter;
	title = "导出";
	strFilter = "Excel文件(*.xls)";
	strFilter += (TCHAR)'\0';   // next string please
	strFilter += _T("*.xls");
	strFilter += (TCHAR)'\0';   // last string
	dlgFile.m_ofn.nMaxCustFilter++;
	dlgFile.m_ofn.nFilterIndex = 1;
	// append the "*.*" all files filter
	CString allFilter;
	VERIFY(allFilter.LoadString(AFX_IDS_ALLFILTER));
	strFilter += allFilter;
	strFilter += (TCHAR)'\0';   // next string please
	strFilter += _T("*.*");
	strFilter += (TCHAR)'\0';   // last string
	dlgFile.m_ofn.nMaxCustFilter++;
	dlgFile.m_ofn.lpstrFilter = strFilter;
	dlgFile.m_ofn.lpstrTitle = title;
	if (dlgFile.DoModal() == IDCANCEL)
		return FALSE; // open cancelled
	sExcelFile.ReleaseBuffer();
	if (MakeSurePathExists(sExcelFile, true))
	{
		if (!DeleteFile(sExcelFile))
		{    // delete the file
			AfxMessageBox("覆盖文件时出错！");
			return FALSE;
		}
	}
	CString str = dlgFile.GetFileTitle();
	sExcelFile = str + _T(".xls");
	return TRUE;
}

//设置坐标轴数值字体参数
void CFilterDlg::setAxisText(CPaintDC& dc) {
	//设置字体
	CFont font;
	font.CreateFont(
		18,                            //字体的高度   
		0,                             //字体的宽度  
		0,                             //nEscapement
		0,                             //nOrientation   
		FW_NORMAL,                     //nWeight   
		FALSE,                         //bItalic   
		FALSE,                         //bUnderline   
		0,                             //cStrikeOut   
		ANSI_CHARSET,                  //nCharSet   
		OUT_DEFAULT_PRECIS,            //nOutPrecision   
		CLIP_DEFAULT_PRECIS,           //nClipPrecision   
		DEFAULT_QUALITY,               //nQuality   
		DEFAULT_PITCH | FF_SWISS,      //nPitchAndFamily     
		_T("宋体"));
	dc.SelectObject(&font);
	dc.SetBkMode(TRANSPARENT);
	dc.SetTextColor(RGB(0,0,0));
}