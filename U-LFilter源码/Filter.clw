; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "filter.h"
LastPage=0

ClassCount=3
Class1=CFilterApp
Class2=CAboutDlg
Class3=CFilterDlg

ResourceCount=2
Resource1=IDD_ABOUTBOX
Resource2=IDD_FILTER_DIALOG

[CLS:CFilterApp]
Type=0
BaseClass=CWinApp
HeaderFile=Filter.h
ImplementationFile=Filter.cpp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=FilterDlg.cpp
ImplementationFile=FilterDlg.cpp

[CLS:CFilterDlg]
Type=0
BaseClass=CDialog
HeaderFile=FilterDlg.h
ImplementationFile=FilterDlg.cpp

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_FILTER_DIALOG]
Type=1
Class=CFilterDlg
ControlCount=33
Control1=IDC_EDIT1,edit,1350639744
Control2=IDC_SPIN1,msctls_updown32,1342177334
Control3=IDC_WAV4,button,1342242816
Control4=IDC_STATIC,static,1342308352
Control5=IDC_EDIT3,edit,1350641792
Control6=IDC_STATIC,static,1342308352
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC4,button,1342178055
Control9=IDC_HORI_SCROLLBAR,scrollbar,1342177280
Control10=IDC_EDIT2,edit,1350639744
Control11=IDC_STATIC3,button,1342178055
Control12=IDC_WAV5,button,1342242816
Control13=IDC_STATIC5,button,1342178055
Control14=IDC_STATIC6,button,1342178055
Control15=IDC_COMSELECT,combobox,1344339971
Control16=IDC_COMSEEPED,combobox,1344339971
Control17=IDC_WAV6,button,1342242816
Control18=IDC_COMBO1,combobox,1344339971
Control19=IDC_STATIC7,button,1342178055
Control20=IDC_STATIC,static,1342308352
Control21=IDC_EDIT4,edit,1350639744
Control22=IDC_STATIC,static,1342308352
Control23=IDC_EDIT5,edit,1350639744
Control24=IDC_STATIC,static,1342308352
Control25=IDC_EDIT6,edit,1350639744
Control26=IDC_STATIC,static,1342308352
Control27=IDC_EDIT7,edit,1350639744
Control28=IDC_STATIC8,button,1342178055
Control29=IDC_COMBO2,combobox,1344339971
Control30=IDC_STATIC1,button,1342178055
Control31=IDC_COMBO3,combobox,1344339971
Control32=IDC_STATIC,static,1342308352
Control33=IDC_STATIC,static,1342308352

