#include "math.h"
#include <cmath>
#include "dealString.h"
#include "FilterDlg.h"
/////////初始化全局变量
s_gList *s_gFilterListHead;//s-g滤波链表头结点
dataList *AverageCSVdataListHead;//相邻平均链表头结点
listpoint *oringinCSVListHead;//原始DAQ链表头节点
bool showOrigin=false;//是否显示原始DAQcsv数据
int WaveHeight = 600;//波形图高度
int WaveWidth = 1000;//波形图宽度
int gapleft = 60;//距离左边长度
int gaptop = 5;//距离顶部长度
int m_nHorzPos=0;//左侧距离X轴原点位置。
int curveX;//获取数据曲线的X坐标。
int yaxistimes=1;//默认Y轴放大倍数。60==600/10；
//设置背景和曲线的RGB全局变量数组
int bgRGB[3] = {220,220,220 };////背景颜色
int curveRGB[3] = {0,0,255};///滤波曲线图颜色
int filterCurveRGB[3] = {255,0,0};///DAQ曲线图颜色
///声明坐标横纵坐标全局变量，用于判断鼠标左移，右移。
int oldMouseX = 200;
int oldMouseY = 300;
bool isLeftClick = false;

/////////计算滤波后链表数据结构及算法
/////////////////////判断标尺是否到窗口外////////////////////////////
bool CFilterDlg::IsDataOut(double x, double y)
{
	if (x > gapleft + WaveWidth || x < gapleft || y<gaptop || y>gaptop + WaveHeight)
	{
		return true;
	}
	return false;
}

/////////////////////添加定时器处理函数////////////////////////////
void CFilterDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	switch (nIDEvent)
	{
	case 0:
		CRect rect(gapleft, gaptop, WaveWidth + gapleft, WaveHeight + gaptop);
		InvalidateRect(rect);
		break;
	}
	CDialog::OnTimer(nIDEvent);
}

//////颜色变换选项
void CFilterDlg::OnCbnSelchangeColor()
{
	// TODO: 在此添加控件通知处理程序代码
	int colorNumber, optionNumber;
	optionNumber = m_com.GetCurSel();
	colorNumber = m_speed.GetCurSel();
	///////设置背景框颜色
	if ((optionNumber == 0) && (colorNumber == 0))
	{//黑
		bgRGB[0] = 0; bgRGB[1] = 0; bgRGB[2] = 0;
	}
	else if ((optionNumber == 0) && (colorNumber == 1))
	{//白
		bgRGB[0] = 255; bgRGB[1] = 255; bgRGB[2] = 255;
	}
	else if ((optionNumber == 0) && (colorNumber == 2))
	{//灰
		bgRGB[0] = 192; bgRGB[1] = 192; bgRGB[2] = 192;
	}
	else if ((optionNumber == 0) && (colorNumber == 3))
	{//红
		bgRGB[0] = 255; bgRGB[1] = 0; bgRGB[2] = 0;
	}
	else if ((optionNumber == 0) && (colorNumber == 4))
	{//绿
		bgRGB[0] = 0; bgRGB[1] = 255; bgRGB[2] = 0;
	}
	else if ((optionNumber == 0) && (colorNumber == 5))
	{//青
		bgRGB[0] = 0; bgRGB[1] = 255; bgRGB[2] = 255;
	}
	else if ((optionNumber == 0) && (colorNumber == 6))
	{//蓝
		bgRGB[0] = 0; bgRGB[1] = 0; bgRGB[2] = 255;
	}
	else if ((optionNumber == 0) && (colorNumber == 7))
	{//紫
		bgRGB[0] = 255; bgRGB[1] = 0; bgRGB[2] = 255;
	}
	////////设置滤波前数据曲线颜色
	else if ((optionNumber == 1) && (colorNumber == 0))
	{//黑
		curveRGB[0] = 0; curveRGB[1] = 0; curveRGB[2] = 0;
	}
	else if ((optionNumber == 1) && (colorNumber == 1))
	{//白
		curveRGB[0] = 255; curveRGB[1] = 255; curveRGB[2] = 255;
	}
	else if ((optionNumber == 1) && (colorNumber == 2))
	{//灰
		curveRGB[0] = 192; curveRGB[1] = 192; curveRGB[2] = 192;
	}
	else if ((optionNumber == 1) && (colorNumber == 3))
	{//红
		curveRGB[0] = 255; curveRGB[1] = 0; curveRGB[2] = 0;
	}
	else if ((optionNumber == 1) && (colorNumber == 4))
	{//绿
		curveRGB[0] = 0; curveRGB[1] = 255; curveRGB[2] = 0;
	}
	else if ((optionNumber == 1) && (colorNumber == 5))
	{//青
		curveRGB[0] = 0; curveRGB[1] = 255; curveRGB[2] = 255;
	}
	else if ((optionNumber == 1) && (colorNumber == 6))
	{//蓝
		curveRGB[0] = 0; curveRGB[1] = 0; curveRGB[2] = 255;
	}
	else if ((optionNumber == 1) && (colorNumber == 7))
	{//紫
		curveRGB[0] = 255; curveRGB[1] = 0; curveRGB[2] = 255;
	}
	///////设置改变滤波后曲线颜色
	else if ((optionNumber == 2) && (colorNumber == 0))
	{//黑
		filterCurveRGB[0] = 0; filterCurveRGB[1] = 0; filterCurveRGB[2] = 0;
	}
	else if ((optionNumber == 2) && (colorNumber == 1))
	{//白
		filterCurveRGB[0] = 255; filterCurveRGB[1] = 255; filterCurveRGB[2] = 255;
	}
	else if ((optionNumber == 2) && (colorNumber == 2))
	{//灰
		filterCurveRGB[0] = 192; filterCurveRGB[1] = 192; filterCurveRGB[2] = 192;
	}
	else if ((optionNumber == 2) && (colorNumber == 3))
	{//红
		filterCurveRGB[0] = 255; filterCurveRGB[1] = 0; filterCurveRGB[2] = 0;
	}
	else if ((optionNumber == 2) && (colorNumber == 4))
	{//绿
		filterCurveRGB[0] = 0; filterCurveRGB[1] = 255; filterCurveRGB[2] = 0;
	}
	else if ((optionNumber == 2) && (colorNumber == 5))
	{//青
		filterCurveRGB[0] = 0; filterCurveRGB[1] = 255; filterCurveRGB[2] = 255;
	}
	else if ((optionNumber == 2) && (colorNumber == 6))
	{//蓝
		filterCurveRGB[0] = 0; filterCurveRGB[1] = 0; filterCurveRGB[2] = 255;
	}
	else if ((optionNumber == 2) && (colorNumber == 7))
	{//紫
		filterCurveRGB[0] = 255; filterCurveRGB[1] = 0; filterCurveRGB[2] = 255;
	}
	CRect rect(0, 0, WaveWidth + gapleft, WaveHeight + gaptop + 50);
	InvalidateRect(rect);
	return;
}

///////水平滑动条更新数据窗口
void CFilterDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	//TODO: 在此添加消息处理程序代码和/或调用默认值
	int pos = m_horiScrollbar.GetScrollPos();// 获取水平滚动条当前位置
	//pos为滚动条位置，m_nHorzPos为水平轴最左边的刻度值。
	int *p;
	int lineNodeAmount = 0;//链表数据结构节点总量
	p = getLineAmount(csvFilename.c_str());
	lineNodeAmount = *(p);
	int clickStep = 100;//点击则公差为100
	int pageStep = 500;//翻页则公差为500
	switch (nSBCode)
	{
		//如果向左滚动一列，则pos减100
	case SB_LINELEFT:
		pos -= clickStep;
		m_nHorzPos -= clickStep;
		break;
		// 如果向右滚动一列，则pos加100  
	case SB_LINERIGHT:
		pos += clickStep;
		m_nHorzPos += clickStep;
		break;
		// 如果向左滚动一页，则pos减500  
	case SB_PAGELEFT:
		pos -= pageStep;
		m_nHorzPos -= pageStep;
		break;
		// 如果向右滚动一页，则pos加500  
	case SB_PAGERIGHT:
		pos += pageStep;
		m_nHorzPos += pageStep;
		break;
		// 如果滚动到最左端，则pos为1  
	case SB_LEFT:
		pos = 1;
		m_nHorzPos = 1;
		break;
		// 如果滚动到最右端，则pos为nodeAmount 
	case SB_RIGHT:
		pos = lineNodeAmount-20;
		m_nHorzPos = lineNodeAmount-20;
		break;
		// 如果拖动滚动块滚动到指定位置，则pos赋值为nPos的值  
	case SB_THUMBPOSITION:
		pos = nPos;
		m_nHorzPos = nPos;
		break;
	}
	/////限制横轴坐标显示范围
	if (m_nHorzPos <= 0)
	{
		m_nHorzPos = 0;
	};
	if (m_nHorzPos >= lineNodeAmount-1000)
	{
		m_nHorzPos = lineNodeAmount-1000;
	};
	//限制横坐标显示数值范围
	if (pos <= 0)
	{
		pos = 0;
	};
	if (pos >= lineNodeAmount - 1000)
	{
		pos = lineNodeAmount - 1000;
	};

	OnUpdateEdit1();
	// 下面的m_horiScrollbar.SetScrollPos(pos);执行时会第二次进入此函数，最终确定滚动块位置，并且会直接到default分支，所以在此处设置编辑框中显示数值  
	m_horiScrollbar.SetScrollPos(pos);
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

//////设置保存数据图像功能
bool CFilterDlg::dataShot()
{
	CWnd *pDesktop = GetDesktopWindow();
	CDC *pdeskdc = pDesktop->GetDC();
	CRect re;
	//获取窗口的大小
	pDesktop->GetClientRect(&re);
	CBitmap bmp;
	int width = 1608;
	int	height = 972 - 35;
	bmp.CreateCompatibleBitmap(pdeskdc, width, height);
	//创建一个兼容的内存画板
	CDC memorydc;
	memorydc.CreateCompatibleDC(pdeskdc);
	//选中画笔
	CBitmap *pold = memorydc.SelectObject(&bmp);
	//绘制图像
	memorydc.BitBlt(0, 0, width, height, pdeskdc, 0,35, SRCCOPY);
	//获取鼠标位置，然后添加鼠标图像
	CPoint po;
	GetCursorPos(&po);
	HICON hinco = (HICON)GetCursor();
	memorydc.DrawIcon(po.x - 10, po.y - 10, hinco);
	//选中原来的画笔
	memorydc.SelectObject(pold);
	BITMAP bit;
	bmp.GetBitmap(&bit);
	//定义 图像大小（单位：byte）
	DWORD size = bit.bmWidthBytes * bit.bmHeight;
	LPSTR lpdata = (LPSTR)GlobalAlloc(GPTR, size);
	//后面是创建一个bmp文件的必须文件头
	BITMAPINFOHEADER pbitinfo;
	pbitinfo.biBitCount = 32;
	pbitinfo.biClrImportant = 0;
	pbitinfo.biCompression = BI_RGB;
	pbitinfo.biHeight = bit.bmHeight;
	pbitinfo.biPlanes = 1;
	pbitinfo.biSize = sizeof(BITMAPINFOHEADER);
	pbitinfo.biSizeImage = size;
	pbitinfo.biWidth = bit.bmWidth;
	pbitinfo.biXPelsPerMeter = 0;
	pbitinfo.biYPelsPerMeter = 0;
	GetDIBits(pdeskdc->m_hDC, bmp, 0, pbitinfo.biHeight, lpdata, (BITMAPINFO*)&pbitinfo, DIB_RGB_COLORS);
	BITMAPFILEHEADER bfh;
	bfh.bfReserved1 = bfh.bfReserved2 = 0;
	bfh.bfType = ((WORD)('M' << 8) | 'B');
	bfh.bfSize = size + 54;
	bfh.bfOffBits = 54;
	//写入文件
	CFile file;
	string ss = "";
	CString strFileName(ss.c_str());
	strFileName += _T("DataShot\\");
	CreateDirectory((LPCTSTR)strFileName, NULL);
	CTime t = CTime::GetCurrentTime();
	CString tt = t.Format("%Y-%m-%d_%H-%M-%S");
	strFileName += tt;
	strFileName += _T(".bmp");
	if (file.Open((LPCTSTR)strFileName, CFile::modeCreate | CFile::modeWrite))
	{
		file.Write(&bfh, sizeof(BITMAPFILEHEADER));
		file.Write(&pbitinfo, sizeof(BITMAPINFOHEADER));
		file.Write(lpdata, size);
		file.Close();
	}
	GlobalFree(lpdata);
	return true;
}

//////选取滤波算法与是否开启原DAQcsv文件
void CFilterDlg::OnCbnSelchangeFilter()
{
	int waveNumber;
	waveNumber = m_waveFilter.GetCurSel();
	switch (waveNumber)
	{
	case 0:
		OnWav1();
		CurrentWav = 1;
		break;
	case 1:
		OnWav2();
		CurrentWav = 2;
		break;
	case 2:
		OnWav3();
		showOrigin = (!showOrigin);
		if (showOrigin)
		{
			m_waveFilter.DeleteString(2);
			m_waveFilter.InsertString(2, "显示DAQ曲线");
			m_waveFilter.SetCurSel(2);
		}
		else
		{
			m_waveFilter.DeleteString(2);
			m_waveFilter.InsertString(2, "关闭DAQ曲线");
			m_waveFilter.SetCurSel(2);
		}
		CurrentWav=3;
		break;
	}
	return;
}

//选择将要滤波的曲线
void CFilterDlg::OnCbnSelchangeCombo2()
{
	//得到选取的滤波曲线，画曲线图时需要改变相应的滤波曲线
	selectCurve = m_changeCurve.GetCurSel();
}

//选择改变Y轴放大、缩小数值显示范围
void CFilterDlg::OnCbnSelchangeControlY()
{
	// TODO: 在此添加控件通知处理程序代码
	enlargeYaxis = m_controlY.GetCurSel();//获得控制Y轴的放大、缩小开关
}

///////查找上一个零节点
listpoint *findLastMinNode(listpoint *keyPoint) {
	listpoint *findPoint = keyPoint;
	float nodeData;
	while (findPoint != NULL)
	{
		nodeData = fabs(findPoint->information->sensorData0);
		if (nodeData<=1)
		{
			return findPoint;
		}
		findPoint = findPoint->last;
	}
	return NULL;
}

//左键单击处理程序,设置标志位。
void CFilterDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	isLeftClick = true;
	CDialog::OnLButtonDown(nFlags, point);
}

////鼠标左键单击抬起处理程序，设置标志位。
void CFilterDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	isLeftClick = false;
	CDialog::OnLButtonUp(nFlags, point);
}

///////////点击标尺按钮事件程序
void CFilterDlg::OnBnClickedWav5()
{
	CButton *drawCrossButoon;
	drawCross = !drawCross;
	drawCrossButoon = (CButton*)GetDlgItem(IDC_WAV5);
	if (drawCross)
	{
		drawCrossButoon->SetWindowText("关闭数据标尺");
	}
	else
	{
		drawCrossButoon->SetWindowText("打开数据标尺");
	}
	return;
}

////显示节点参数信息
listpoint *serchNode = new listpoint;
void CFilterDlg::showNodeData(int nodeOrder)
{
	CString nodeNumber, oringinData;
	listpoint *searchNode = new listpoint;
	searchNode = serchNode;
	int showNumber = searchNode->information->pointNumber;
	float showOrigin;
	if (selectCurve==0)//选择压边力曲线
	{
		showOrigin = searchNode->information->sensorData0;//显示压边力数值
	}
	else if (selectCurve==1)
	{
		showOrigin = searchNode->information->sensorData1;//显示弯曲力数值
	}
	////编辑框中显示过滤前数据
	oringinData.Format(_T("%6.3f"), showOrigin);
	SetDlgItemText(IDC_EDIT4, oringinData);
	////编辑框中显示节点number数据
	nodeNumber.Format(_T("%d"), showNumber);
	SetDlgItemText(IDC_EDIT6, nodeNumber);
}

//////鼠标移动函数
void CFilterDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	SetTimer(0, 30, NULL);/////人类视觉最小感知时间（绝大多数人）。
	int mouseX, mouseY;
	mouseX = point.x;
	mouseY = point.y;
	////将鼠标置于数据显示框中
	if (mouseX <= gapleft)
	{
		mouseX = gapleft + 5;
	}
	else if (mouseX >= gapleft + WaveWidth)
	{
		mouseX = gapleft + WaveWidth - 5;
	};
	if (mouseY <= gaptop)
	{
		mouseY = gaptop + 5;
	}
	else if (mouseY >= gaptop + WaveHeight)
	{
		mouseY = gaptop + WaveHeight - 5;
	};
	////判断鼠标左右移动；
	if (mouseX - oldMouseX > 0)//鼠标右移
	{
		mouseX++;
	}
	else if (mouseX - oldMouseX < 0)////鼠标左移
	{
		mouseX--;
	};
	oldMouseX = mouseX;///用于判断鼠标左、右移。
	m_mouseMove.x = mouseX;////得到目前鼠标的坐标
	m_mouseMove.y = mouseY;
	curveX = m_nHorzPos + mouseX - gapleft;/////曲线的X坐标为全局变量。
	CDialog::OnMouseMove(nFlags, point);
}

/////////设置标尺线
void CFilterDlg::Draw_CrossLine(CDC *pDC)
{
	CPen p;
	p.CreatePen(PS_DASH, 0, RGB(filterCurveRGB[0], filterCurveRGB[1], filterCurveRGB[2]));
	pDC->SelectObject(&p);
	int x, y;
	x = m_mouseMove.x;
	/////收索对应X节点的Y值
	serchNode = oringinCSVListHead->next;
	while (serchNode->next!= NULL)///此时不能查找最后一个节点值
	{
		if (serchNode->information->pointOrder ==curveX)
		{
			break;///当搜索成功时，serchNode即为需用节点。
		}
		else
		{
			serchNode = serchNode->next;
		}
	}
	int radius = 5;//选择数据点的显示半径
	//选择压边力、弯曲力曲线节点并显示数据
	if (selectCurve==0)//使用压边力数据曲线
	{
		if (enlargeYaxis == 0)//缩小Y轴数值显示范围
		{
			y = (WaveHeight / 2 + gaptop+ radius) - ((serchNode->information->sensorData0))*yaxistimes;
		}
		else if (enlargeYaxis == 1)//放大Y轴数值显示范围
		{
			//放大则CSV数值应该除以放大倍数
			y = (WaveHeight / 2 + gaptop + radius) - ((serchNode->information->sensorData0)) / yaxistimes;
		}
	}
	else if(selectCurve==1)//使用弯曲力数据曲线
	{
		if (enlargeYaxis==0)//缩小Y轴数值显示范围
		{
			y = (WaveHeight / 2 + gaptop + radius) -((serchNode->information->sensorData1))*yaxistimes;
		}
		else if (enlargeYaxis==1)//放大Y轴数值显示范围
		{
			//放大则CSV数值应该除以放大倍数
			y = (WaveHeight / 2 + gaptop + radius) - ((serchNode->information->sensorData1)) / yaxistimes;
		}
	}
	////画选择数据点
	CBrush Brush;
	Brush.CreateSolidBrush(RGB(filterCurveRGB[0], filterCurveRGB[1], filterCurveRGB[2]));
	pDC->BeginPath();
	pDC->Ellipse(x-radius, y-radius,x+radius,y+radius);
	pDC->EndPath();
	pDC->SelectObject(&Brush);
	pDC->FillPath();
	////画数据标尺线
	CPoint w_s, w_e, h_s, h_e;
	w_s.x = gapleft;
	w_s.y = y;
	w_e.x = gapleft + WaveWidth;
	w_e.y = y;
	h_s.x = x;
	h_s.y = gaptop + radius;
	h_e.x = x;
	h_e.y = gaptop + radius + WaveHeight;
	pDC->MoveTo(w_s);
	pDC->LineTo(w_e);
	pDC->MoveTo(h_s);
	pDC->LineTo(h_e);
	showNodeData(curveX);////将数据显示出来
	p.DeleteObject();
}

//导出数据采集数据（输入数据区间） 
listpoint *outDAQData(listpoint *CSVDataHead, int startPointNumber, int endPointNumber)
{
	listpoint *searchPoint = CSVDataHead->next;
	listpoint *SelectStartPoint, *SelectEndPoint;
	//查找截取的数据段开始点
	while (searchPoint->next != NULL)//此时不能查找最后一个节点值
	{
		if (searchPoint->information->pointNumber == startPointNumber)
		{
			SelectStartPoint = searchPoint;//返回查找到的节点值
			break;
		}
		else
		{
			searchPoint = searchPoint->next;
		}
	}
	//初始化xls
	CSaveToXsl xls;
	xls.GetFileName("");
	int number = searchPoint->information->pointNumber;
	string time = searchPoint->information->pointTime;
	CString DAQtime = time.c_str();
	float DAQdata = 0.00;
	if (selectCurve==0)
	{
		DAQdata = searchPoint->information->sensorData0;
	}
	else
	{
		DAQdata = searchPoint->information->sensorData1;
	}
	xls.Initialize("选择的DAQ数据段", "数据点 int,采集时间 TEXT,DAQ数据 float", "数据点,采集时间,DAQ数据");
	//得到了截取数据段的开始节点,通过链表节点的pointNumber设置截取数据段的边界条件
	xls.InsertToTable(number, DAQtime, DAQdata);
	//由于截取的数据段量较小，可以采用选择分支语句。
	while ((searchPoint->next != NULL) && ((searchPoint->information->pointNumber) != endPointNumber))
	{
		searchPoint = searchPoint->next;
		//将DAQ数据段内的数据放入excel文件中
		number = searchPoint->information->pointNumber;
		time = searchPoint->information->pointTime;
		CString DAQtime = time.c_str();
		if (selectCurve==0)//选择压边力曲线
		{
			DAQdata = searchPoint->information->sensorData0;
		}
		else{//选择弯曲力曲线
			DAQdata = searchPoint->information->sensorData1;
		}
		xls.InsertToTable(number, DAQtime, DAQdata);//插入一行到表中
	}
	return SelectStartPoint;
}

//导出DAQ数据段
void CFilterDlg::OnBnClickedWav6()
{
	CString startPointNumber, endPointNumber;
	m_edit5.GetWindowText(startPointNumber);
	m_edit7.GetWindowText(endPointNumber);
	int startNumber = atoi(startPointNumber);
	int endNumber = atoi(endPointNumber);
	if (startNumber > endNumber)
	{
		swap(startNumber, endNumber);
	}
	//导出DAQ链表中选中的数据为Execl格式
    outDAQData(oringinCSVListHead,startNumber,endNumber);
}

//设置程序关闭时，程序调用的函数
LRESULT CFilterDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_SYSCOMMAND:
	{
		if (wParam == SC_CLOSE)
		{
			AfxGetMainWnd()->SendMessage(WM_CLOSE);
			//关闭程序后处理后续问题
			char* filePath = "useData\\";
			if (_access(filePath, 0) == -1)///如果没有useData文件夹
			{
				CString strFileName = "useData\\";
				cout << "计算S_G滤波算法所需文件\n计算S_G滤波算法所需文件" << endl;
				CreateDirectory((LPCTSTR)strFileName, NULL);//没有文件夹就创建文件夹
			}
			//删除useData下面的afterFilter.csv文件,方便下次使用s_g滤波算法
			string command_rd = "del useData\\*.csv";
			system(command_rd.c_str());
			//调用cmd,将当前用到的csv文件移动到useData文件夹下，以免下次用软件的时候当前目录出现两个csv文件
			command_rd = "move  \""+ csvFilename+"\" useData\\";
			system(command_rd.c_str());
			//调用cmd让程序再次自动运行
			string restart = "cmd.exe /c ping -n 10 127.1>nul&Filter.exe";
			WinExec(restart.c_str(), MB_OK);
			DestroyWindow();//关闭窗口
		}
		break;
	}
	}
	return CDialog::WindowProc(message, wParam, lParam);
}