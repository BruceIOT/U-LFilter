import sys#引入sys的python包
import numpy as np#引入计算python包
import pandas as pd#pandas包用于对csv文件的处理
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter#scipy包中有Savitzky-Golay滤波函数
#下面用S-G滤波进行滤波操作，csvFileName为cmd调用第一个参数名称
csvFileName=sys.argv[1]#接受cmd传入的csv文件名字符串
#读取滤波前的csv文件并保存为dataFrame数据结构，2为压边力，3为弯曲力
CSVdata=pd.read_csv(csvFileName,header=None,delimiter=",",usecols=[0,1,2,3],encoding='unicode_escape',skiprows=10)
bhfFilterData=savgol_filter(CSVdata[2],11,5)#对压边力过滤。
filterData= savgol_filter(CSVdata[3], 11,5) #对弯曲力过滤，使用Savitzky-Golay滤波函数，window size 11, polynomial order 5。
print("length of array:",len(filterData))
#将DataFrame存储为csv,index表示是否显示行名，default=True
dataframe = pd.DataFrame({'index,pointNumber':CSVdata[0],'pointTime':CSVdata[1],'bhfOringinData':CSVdata[2],'bhfFilterData':bhfFilterData,'bendOriginData':CSVdata[3],'bendFilterData':filterData})
dataframe.to_csv("./useData/afterFilter.csv",index=True,sep=',')#header=False时，可以将列索引去掉，index=False，可以将列索引去掉
