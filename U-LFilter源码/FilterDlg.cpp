#include "stdafx.h"
#include "Filter.h"
#include "FilterDlg.h"
#include "itemDeal.h"
#include "math.h"
#include <conio.h>

int *p;//得到csv文件的行数，列数
int lineNodeAmount;//链表数据结构节点总量
//////声明链表重要数据结构体,全局可用
listpoint *CSVMax=new listpoint;

CWnd *pWnd;
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	// Dialog Data
		//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}
void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}
BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// CFilterDlg dialog，此为构造函数，程序运行时最先执行
CFilterDlg::CFilterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFilterDlg::IDD, pParent)
{
	///最开始执行的各种函数
	getDAQname();//检测是否存在csv文件
	p = getLineAmount(csvFilename.c_str());//计算csv文件行、列数
	lineNodeAmount = *(p);//获取csv文件总行数
	oringinCSVListHead = listSensorData(lineNodeAmount);//将csv文件导入链表，传入参数为csv文件的总行数
	calCSVS_G();//计算S-G滤波算法CSV文件
	//初始化计算凸模速度函数值
	
	//{{AFX_DATA_INIT(CFilterDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	DataSuccess = false;//是否自动画图
}

void CFilterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFilterDlg)
	DDX_Control(pDX, IDC_EDIT3, m_Edit3);
	DDX_Control(pDX, IDC_EDIT2, m_Edit2);
	DDX_Control(pDX, IDC_EDIT1, m_ZoominEdit);
	DDX_Control(pDX, IDC_SPIN1, m_ZoominSpin);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_HORI_SCROLLBAR, m_horiScrollbar);
	DDX_Control(pDX, IDC_COMSELECT, m_com);
	DDX_Control(pDX, IDC_COMSEEPED, m_speed);
	DDX_Control(pDX, IDC_COMBO1, m_waveFilter);
	DDX_Control(pDX, IDC_EDIT4, m_edit4);
	DDX_Control(pDX, IDC_EDIT5, m_edit5);
	DDX_Control(pDX, IDC_EDIT6, m_edit6);
	DDX_Control(pDX, IDC_WAV6, m_exportDAQ);
	DDX_Control(pDX, IDC_EDIT7, m_edit7);
	DDX_Control(pDX, IDC_COMBO2, m_changeCurve);
	DDX_Control(pDX, IDC_COMBO3, m_controlY);
}

BEGIN_MESSAGE_MAP(CFilterDlg, CDialog)
	//{{AFX_MSG_MAP(CFilterDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_WAV1, OnWav1)
	ON_EN_UPDATE(IDC_EDIT1, OnUpdateEdit1)
	ON_BN_CLICKED(IDC_WAV2, OnWav2)
	ON_EN_UPDATE(IDC_EDIT2, OnUpdateEdit2)
	ON_BN_CLICKED(IDC_WAV3, OnWav3)
	ON_EN_UPDATE(IDC_EDIT3, OnUpdateEdit3)
	ON_BN_CLICKED(IDC_WAV4, OnWav4)
	//}}AFX_MSG_MAP
	ON_WM_HSCROLL()
	ON_WM_ERASEBKGND()
	ON_CBN_SELCHANGE(IDC_COMSEEPED, CFilterDlg::OnCbnSelchangeColor)
	ON_CBN_SELCHANGE(IDC_COMBO1, CFilterDlg::OnCbnSelchangeFilter)
	ON_WM_TIMER()
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_WAV5, CFilterDlg::OnBnClickedWav5)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_BN_CLICKED(IDC_WAV6, CFilterDlg::OnBnClickedWav6)
	ON_CBN_SELCHANGE(IDC_COMBO2,CFilterDlg::OnCbnSelchangeCombo2)
	ON_CBN_SELCHANGE(IDC_COMBO3,CFilterDlg::OnCbnSelchangeControlY)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFilterDlg message handlers
void CFilterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

///////////重载WM_OnEraseBkgnd，防止操作时屏幕闪烁
BOOL CFilterDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	return true;
	return CDialog::OnEraseBkgnd(pDC);
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CFilterDlg::OnQueryDragIcon()
{
	return (HCURSOR)m_hIcon;
}

BOOL CFilterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	// Add "About..." menu item to system menu.
	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);
	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	/////////////////////////初始化/////////////////////
	//Y轴的缩小、放大
	//首先是缩小范围
	CEdit* zoomin_edit = (CEdit*)GetDlgItem(IDC_EDIT1);
	m_ZoominSpin.SetBuddy(zoomin_edit);
	m_ZoominSpin.SetRange(1, 50);//Y放大倍数
	m_ZoominSpin.SetPos(1);//Y轴默认显示缩放数字
	m_Zoomin = yaxistimes; //Y轴默认缩放倍数,将缩放倍数传递给m_zoomin变量
	//下面为放大范围
	
	//Y轴的缩小、放大初始化完毕
	EnableRegulate(false);
	pWnd = GetDlgItem(IDC_STATIC1);
	ShowWindow(SW_MAXIMIZE);
	m_horiScrollbar.SetScrollRange(0, lineNodeAmount);//
	//设置水平滚动条的初始位置为100   
	m_horiScrollbar.SetScrollPos(0);
	//设置三个编辑框中默认显示选项
	m_com.SetCurSel(0);
	m_speed.SetCurSel(2);
	m_waveFilter.SetCurSel(0);//选取滤波算法
	m_changeCurve.SetCurSel(0);//选取压边力数据集
	m_controlY.SetCurSel(0);//设置Y轴默认为缩小
	//初始化鼠标相关变量
	m_mouseMove.x = 0;
	m_mouseMove.y = 0;
	drawCross = false;//是否自动画十字线数据标尺
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
// to draw the icon.  For MFC applications using the document/view model,
// this is automatically done for you by the framework.
void CFilterDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	if (IsIconic())
	{
		SendMessage(WM_ICONERASEBKGND, (WPARAM)dc.GetSafeHdc(), 0);
		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		if (DataSuccess) {
			DrawOldWave(dc);
		}
		CDialog::OnPaint();
	}
	////////初始化标尺
	if (drawCross)
	{
		Draw_CrossLine(&dc);
	}
	/////////////////////////绘制坐标轴函数////////////
	CDC *pDC = &dc;
	CString str;
	int i, j;
	int x, y;
	int markLen = 10;///刻度线长度
	//强制更新绘图, 不可少, 否则绘图会出错
	//使static控件区域无效
	pWnd->Invalidate();
	//更新窗口, 此时才真正向系统发送重绘消息, 没有这句,会出问题
	pWnd->UpdateWindow();
	CPen *pPenRed = new CPen();//创建画笔对象
	pPenRed->CreatePen(PS_SOLID,2,RGB(0, 0, 0)); //坐标轴黑色画笔
	CPen *pPen = NULL;
	//选中当前红色画笔,并保存以前的画笔
	CGdiObject *pOldPen = pDC->SelectObject(pPenRed);
	pDC->MoveTo(gapleft, gaptop); //绘制坐标轴
	pDC->LineTo(gapleft, WaveHeight + gaptop); //竖起轴
	pDC->LineTo(gapleft + WaveWidth, WaveHeight + gaptop); //水平轴
	//设置坐标轴数值更新
	CBrush brush(RGB(255, 255, 255));//填充数值背景颜色
	CRect verticalTextRect(0, 0, gapleft, gaptop + WaveHeight + 300);//纵坐标数字显示区域
	CRect horiTextRect(0, gaptop + WaveHeight, WaveWidth + gapleft, gaptop + WaveHeight + 100);//横坐标数字显示区域
	dc.FillRect(horiTextRect, &brush);
	dc.FillRect(verticalTextRect, &brush);
	setAxisText(dc);
	//写X轴刻度值
	for (i = 0;i<=10; i++)///设置十个刻度区间
	{
		str.Format("%6.1f", float(m_nHorzPos+100*i));
		pDC->TextOut(gapleft + i * 100 - 35, WaveHeight + gaptop, str);
		//绘制X轴刻度
		pDC->MoveTo(i * 100 + gapleft, WaveHeight + gaptop - markLen);
		pDC->LineTo(i * 100 + gapleft, WaveHeight + gaptop);
	}
	//写Y轴刻度值
	for (i = 0; i <= 10; i++)
	{
		float m;
		if (i < 6)
		{
			m = -5 + i;
		}
		else
		{
			m = i - 5;
		};
		//防止除数为0
		if (yaxistimes == 0)
		{
			yaxistimes = 1;
		};
		float tolerance=60;//设置默认等差数列公差
		if (enlargeYaxis==0)//缩小Y轴数值
		{
			tolerance = (float)WaveHeight/(yaxistimes * 10);//计算Y轴等差数列公差
		}
		else if (enlargeYaxis==1)//放大Y轴数值
		{
			tolerance = (float)((WaveHeight*yaxistimes)/10);//计算Y轴等差数列公差
		}
		//Y轴不同放大倍数，不同刻度值。
		str.Format("%6.1f", m*(tolerance));
		pDC->TextOut(0, WaveHeight -(60*i+5), str);//将数值显示到正确位置
		//绘制Y轴刻度
		pDC->MoveTo(gapleft +markLen, WaveHeight + gaptop - 60 * i);
		pDC->LineTo(gapleft,WaveHeight + gaptop - 60 * i);
	}
	//绘制X箭头
	pDC->MoveTo(WaveWidth + gapleft - markLen, WaveHeight + gaptop - 5);
	pDC->LineTo(WaveWidth + gapleft, WaveHeight + gaptop);
	pDC->LineTo(WaveWidth + gapleft - markLen, WaveHeight + gaptop + 5);
	//绘制Y箭头
	pDC->MoveTo(gapleft - 5, gaptop + markLen);
	pDC->LineTo(gapleft, gaptop); //绘制左边箭头
	pDC->LineTo(gapleft + 5, gaptop + markLen); //绘制右边箭头
	//恢复以前的画笔
	pDC->SelectObject(pOldPen);
	delete pPenRed;
	if (pPen != NULL)
	{
		delete pPen;
	}
	ReleaseDC(pDC);
}
//绘制曲线图
void CFilterDlg::DrawOldWave(CPaintDC& dc)
{
	///////界面相关变量声明
	CBitmap bitmap;
	CBitmap* pOldBitmap;
	CDC MemDC;
	/////显示矩形框参数，决定背景颜色显示区间,bgRGB[3],curveRGB[3],filterCurveRGB[3]
	CRect rect(0, 0, lineNodeAmount + gapleft, WaveHeight + gaptop);
	CBrush brush(COLORREF(RGB(bgRGB[0], bgRGB[1], bgRGB[2])));//背景颜色
	CBrush* oldbrush;
	CPen pen, pen1;
	CPen* oldpen;
	//画图
	MemDC.CreateCompatibleDC(&dc);//建立与显示设备兼容的内存设备场境
	bitmap.CreateCompatibleBitmap(&dc,lineNodeAmount,1000);///波形图显示长度，宽度
	pOldBitmap = MemDC.SelectObject(&bitmap); ///将位图选入内存场境
	pen.CreatePen(PS_SOLID, 1, RGB(curveRGB[0], curveRGB[1], curveRGB[2]));////滤波曲线颜色
	oldpen = MemDC.SelectObject(&pen);
	oldbrush = MemDC.SelectObject(&brush);
	MemDC.FillRect(rect, &brush);/////将矩形框填充选中颜色
	float ny;
	float startY = (WaveHeight / 2) + gaptop;////原点的Y坐标
	MemDC.MoveTo(0, startY);//第一个显示的数据点
	//用S_G滤波法滤波后的数据显示压边、弯曲滤波曲线
	if (CurrentWav ==1)
	{
		s_gList *CSVDrawPoint = s_gFilterListHead->nextNode;
		int t = 0;
		if (selectCurve==0)//使用压边力滤波数据绘制曲线
		{//为了简绍分支决策，提高程序运行效率，采用两个if
			if (enlargeYaxis==0)//缩小Y轴数值显示范围
			{
				while (CSVDrawPoint->nextNode != NULL)
				{
					ny = (float)(CSVDrawPoint->bhfFilterData*m_Zoomin);
					MemDC.LineTo(t, startY - ny);
					CSVDrawPoint = CSVDrawPoint->nextNode;
					t++;//此时代表x轴的数据自增1
				}
			}
			else if (enlargeYaxis==1)//放大Y轴数值显示范围
			{
				while (CSVDrawPoint->nextNode != NULL)
				{
					ny = (float)((CSVDrawPoint->bhfFilterData)/m_Zoomin);
					MemDC.LineTo(t, startY - ny);
					CSVDrawPoint = CSVDrawPoint->nextNode;
					t++;//此时代表x轴的数据自增1
				}
			}
		}
		else if (selectCurve==1)//使用弯曲力滤波数据绘制曲线
		{
			if (enlargeYaxis==0)//缩小Y轴显示范围
			{
				while (CSVDrawPoint->nextNode != NULL)
				{
					ny = (float)(CSVDrawPoint->filterData *m_Zoomin);
					MemDC.LineTo(t, startY - ny);
					CSVDrawPoint = CSVDrawPoint->nextNode;
					t++;//此时代表x轴的数据自增1
				}
			}
			else if (enlargeYaxis==1)//放大Y轴数值显示范围
			{
				while (CSVDrawPoint->nextNode != NULL)
				{
					ny = (float)((CSVDrawPoint->filterData)/m_Zoomin);
					MemDC.LineTo(t, startY - ny);
					CSVDrawPoint = CSVDrawPoint->nextNode;
					t++;//此时代表x轴的数据自增1
				}
			};
		};
	}
	else if (CurrentWav == 2)//相邻平均滤波法
	{
		//相邻平均头节点无数据
		dataList *CSVDrawPoint = AverageCSVdataListHead->nextNode;;
		int t = 0;
		if (selectCurve==0)//选择压边力滤波曲线
		{
			if (enlargeYaxis ==0)//缩小Y轴数值
			{
				while (CSVDrawPoint->nextNode != NULL)
				{
					ny = (float)(CSVDrawPoint->bhfFilterData *m_Zoomin);//压边力滤波值
					MemDC.LineTo(t, startY - ny);
					CSVDrawPoint = CSVDrawPoint->nextNode;
					t++;
				}
			}
			else if (enlargeYaxis ==1)//放大Y轴数值
			{
				while (CSVDrawPoint->nextNode != NULL)
				{
					ny = (float)((CSVDrawPoint->bhfFilterData)/m_Zoomin);//压边力滤波值
					MemDC.LineTo(t, startY - ny);
					CSVDrawPoint = CSVDrawPoint->nextNode;
					t++;
				}
			};
		}
		else if(selectCurve==1)//选择显示弯曲力滤波曲线
		{
			if (enlargeYaxis==0)//缩小Y轴数值
			{
				while (CSVDrawPoint->nextNode != NULL)//缩小Y轴数值
				{
					ny = (float)(CSVDrawPoint->filterNodeData*m_Zoomin);//s_g滤波数据
					MemDC.LineTo(t, startY - ny);
					CSVDrawPoint = CSVDrawPoint->nextNode;
					t++;
				}
			}
			else if (enlargeYaxis==1)//放大Y轴数值
			{
				while (CSVDrawPoint->nextNode != NULL)
				{
					ny = (float)((CSVDrawPoint->filterNodeData)/m_Zoomin);//s_g滤波数据
					MemDC.LineTo(t, startY - ny);
					CSVDrawPoint = CSVDrawPoint->nextNode;
					t++;
				}
			};
		};
	};
	///////////////////////////关闭或者显示原始波形图。////////////
	if (showOrigin)
	{
		//删除原来的画笔
		MemDC.SelectObject(oldpen);
		pen.DeleteObject();
		//重新创建滤波前画笔
		pen.CreatePen(PS_SOLID, 1, RGB(filterCurveRGB[0], filterCurveRGB[1], filterCurveRGB[2]));////DAQ曲线颜色
		oldpen = MemDC.SelectObject(&pen);
		/////显示DAQami数据曲线图
		MemDC.MoveTo(0, startY);//第一个显示的数据点
		//////////////////////画滤波前波形（即原始数据波形）////////////////
		listpoint *CSVDrawPoint = oringinCSVListHead->next;//原CSV头结点无数据
		int t = 0;
		if (selectCurve==0)//显示压边力原曲线
		{
			if (enlargeYaxis==0)//缩小Y轴数值
			{
				while (CSVDrawPoint->next != NULL)
				{
					ny = (float)(CSVDrawPoint->information->sensorData0 *m_Zoomin);
					MemDC.LineTo(t, startY - ny);
					CSVDrawPoint = CSVDrawPoint->next;
					t++;
				}
			}
			else if (enlargeYaxis==1)//放大Y轴显示范围
			{
				while (CSVDrawPoint->next != NULL)
				{
					ny = (float)((CSVDrawPoint->information->sensorData0)/m_Zoomin);
					MemDC.LineTo(t, startY - ny);
					CSVDrawPoint = CSVDrawPoint->next;
					t++;
				}
			};
		}
		else if (selectCurve==1)//显示弯曲力原曲线。
		{
			if (enlargeYaxis==0)//缩小Y轴数值显示范围
			{
				while (CSVDrawPoint->next != NULL)
				{
					ny = (float)(CSVDrawPoint->information->sensorData1*m_Zoomin);
					MemDC.LineTo(t, startY - ny);
					CSVDrawPoint = CSVDrawPoint->next;
					t++;
				}
			}
			else if (enlargeYaxis==1)//放大Y轴显示范围
			{
				while (CSVDrawPoint->next!= NULL)
				{
					ny = (float)((CSVDrawPoint->information->sensorData1)/m_Zoomin);
					MemDC.LineTo(t, startY - ny);
					CSVDrawPoint = CSVDrawPoint->next;
					t++;
				}
			};
		};
	};
	///////////////////////画出波形图////////////////////////
	dc.StretchBlt(gapleft, gaptop, rect.Width(), rect.Height(), &MemDC, m_nHorzPos, 0, rect.Width(), rect.Height(), SRCCOPY);
	//dc.BitBlt(gapleft,gaptop,lineNodeAmount,WaveHeight,&MemDC,0,0,SRCCOPY);
	//////销毁对象，释放内存。
	MemDC.SelectObject(oldpen);
	pen.DeleteObject();
	MemDC.SelectObject(oldbrush);
	brush.DeleteObject();
	MemDC.SelectObject(pOldBitmap);
}

/////////////////////////////实现滤波相关算法////////////////////////////
void CFilterDlg::OnWav1()////得到S-G滤波的链表
{
	EnableRegulate(true);
	CalWav1();
	UpdateStaticBox(1);
	CRect rect(gapleft, gaptop, WaveWidth + gapleft, WaveHeight + gaptop);
	InvalidateRect(rect);
	CurrentWav = 1;
}

void CFilterDlg::OnWav2()//调用相邻平均滤波法
{
	// TODO: Add your control notification handler code here
	EnableRegulate(true);
	CalWav2();
	UpdateStaticBox(2);
	CRect rect(gapleft, gaptop, WaveWidth + gapleft, WaveHeight + gaptop);
	InvalidateRect(rect);
	CurrentWav = 2;
}

void CFilterDlg::OnWav3() ////显示原csv数据调用
{
	// TODO: Add your control notification handler code here
	EnableRegulate(true);////频率开关控制
	CalWav3();////调用滤波法3
	/////下面皆为画图
	UpdateStaticBox(3);/////设置滤波框左上角的值////////
	CRect rect(gapleft, gaptop, WaveWidth + gapleft, WaveHeight + gaptop);
	InvalidateRect(rect);////让矩形框生效。
	CurrentWav = 3;////当前波形。
}

//此函数的唯一作用为改变DataSuccess变量的标志。
void CFilterDlg::CalWav1()
{
	s_gFilterListHead = getS_GListCSV();
	DataSuccess=true;
}

////////////////////////运用相邻平均法对原始CSV数据进行滤波操作。////////////////
void CFilterDlg::CalWav2()
{
	//对csv数据进行相邻平均滤波并将头结点指针传给全局变量
	AverageCSVdataListHead = calDataList(oringinCSVListHead);
	DataSuccess = true;
}

//////////////////////////////调用显示原始的CSV数据算法。////////////////////////
void CFilterDlg::CalWav3()
{
	////设置滤波标志为true
	DataSuccess = true;
}

/////调用截屏函数
void CFilterDlg::OnWav4()
{
	bool result = dataShot();
	if (result)
	{
		system("start DataShot");//同时打开截图文件夹
	}
}

////Y轴放大倍数更新函数
void CFilterDlg::OnUpdateEdit1()
{
	if (IsWindowVisible())
	{
		CString text = "1";
		m_ZoominEdit.GetWindowText(text);
		///////将Y轴放大倍数按钮放大十倍，主要是为了方便实验人员快速观察波形图
		m_Zoomin = atoi(text);
		yaxistimes = m_Zoomin;
		CRect rect(0, 0, WaveWidth + gapleft, WaveHeight + gaptop + 100);
		InvalidateRect(rect);
	}
}

//计算显示凸模速度功能函数
void CFilterDlg::OnUpdateEdit2()
{
	float punchDisplaceMent, moveTime, punchMoveSpeed;
	punchDisplaceMent = moveTime = punchMoveSpeed=1;
	CString convertString, text;
    convertString=text = "1";
	listpoint *lastMinNode = new listpoint;
	oringinCSVListHead = oringinCSVListHead->next;
	CSVMax = getCSVMax(oringinCSVListHead);
	lastMinNode = findLastMinNode(CSVMax);
	moveTime =(float)((CSVMax->information->pointNumber)- (lastMinNode->information->pointNumber));///单位为秒
	m_Edit2.GetWindowText(text);
	punchDisplaceMent = (atof(text));///单位为米
	punchMoveSpeed = (punchDisplaceMent/moveTime) * 10;

	convertString.Format(_T("%6.3f"), punchMoveSpeed);
	SetDlgItemText(IDC_EDIT3, convertString);
}

void CFilterDlg::OnUpdateEdit3()
{
	if (IsWindowVisible()) {
		if (m_Edit3.IsWindowEnabled()) {
			//OnWav2();
		}
	}
}

void CFilterDlg::EnableRegulate(bool enable)
{
	CEdit *edit2, *edit3;
	edit2 = (CEdit*)GetDlgItem(IDC_EDIT2);
	edit3 = (CEdit*)GetDlgItem(IDC_EDIT3);
	if (edit2)
		edit2->EnableWindow(enable);
	if (edit3)
		edit3->EnableWindow(enable);
}

////////////////////////曲线框上方提示文字方法///////////////////////
void CFilterDlg::UpdateStaticBox(int status)
{
	CStatic *static1, *static2;
	static1 = (CStatic*)GetDlgItem(IDC_STATIC1);
	static2 = (CStatic*)GetDlgItem(IDC_STATIC2);
	if (status == 1)
	{
		static1->SetWindowText("S-G滤波器曲线图");
	}
	else if (status == 2)
	{
		static1->SetWindowText("相邻平均滤波器曲线图");
	}
	else if (status == 3) {
		static1->SetWindowText("显示DAQ数据曲线图（滤波前）");
	}
}
