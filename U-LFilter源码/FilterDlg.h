#if !defined(AFX_FILTERDLG_H__08E648B9_BCF5_11D3_B1BC_0080C84821E0__INCLUDED_)
#define AFX_FILTERDLG_H__08E648B9_BCF5_11D3_B1BC_0080C84821E0__INCLUDED_
#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
static double *y = new double[50000];////////此为滤波数据数组。
//static double *averageFilterData = new double[50000];////////此为相邻平均法滤波数据数组。
static double *s_g = new double[50000];////////此为S-G滤波法滤波数据数组。
/////////////////////////////////////////////////////////////////////////////
// CFilterDlg dialog

class CFilterDlg : public CDialog
{
// Construction
public:
	void UpdateStaticBox(int status);
	void CalWav3();
	void ScreenShot(void);
	void CalWav4();
	void CalWav2();
	void CalWav1();
	void DrawOldWave(CPaintDC& dc);
	CFilterDlg(CWnd* pParent = NULL);  // standard constructor
	double y2[501],y4[14][501],v[2][501];
	int CurrentWav;

// Dialog Data
	//{{AFX_DATA(CFilterDlg)
	enum { IDD = IDD_FILTER_DIALOG };
	CEdit	m_Edit3;
	CEdit	m_Edit2;
	CEdit	m_ZoominEdit;
	CSpinButtonCtrl	m_ZoominSpin;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFilterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void EnableRegulate(bool enable);
	bool DataSuccess;
	HICON m_hIcon;
	int m_Zoomin;
	// Generated message map functions
	//{{AFX_MSG(CFilterDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnWav1();
	void OnUpdateEdit1();
	afx_msg void OnWav2();
	afx_msg void OnUpdateEdit2();
	afx_msg void OnUpdateEdit3();
	afx_msg	bool dataShot();
	afx_msg void OnWav4();
	afx_msg void OnWav3();
	afx_msg void OnStatic1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CScrollBar m_horiScrollbar;
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnBnClickedStatic5();
	CComboBox m_com;
	CComboBox m_speed;
	afx_msg void OnCbnSelchangeColor();
	CComboBox m_waveFilter;
	afx_msg void OnCbnSelchangeFilter();
	void Draw_CrossLine(CDC * pDC);
	bool IsDataOut(double x, double y);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	CPoint m_mouseMove, m_mouseMovey,leftDownPoint;
	afx_msg void OnBnClickedWav5();
	void showNodeData(int nodeOrder);
	CDC *pDC;
	bool drawCross;
	void calCSVS_G();
	void setAxisText(CPaintDC & dc);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	CEdit m_edit4;
	CEdit m_edit5;
	CEdit m_edit6;
	CButton m_exportDAQ;
	afx_msg void OnBnClickedWav6();
	LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	CEdit m_edit7;
	CComboBox m_changeCurve;
	afx_msg void OnCbnSelchangeCombo2();
	CComboBox m_controlY;
	afx_msg void OnCbnSelchangeControlY();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILTERDLG_H__08E648B9_BCF5_11D3_B1BC_0080C84821E0__INCLUDED_)
